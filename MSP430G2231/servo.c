/*
 * servo.c
 *
 *  Created on: Mar 23, 2021
 *      Author: Shesley LECKPA
 */

#include "msp430.h"
#include "servo.h"
#include "typedef.h"
/*
cr�e un retard de l'ordre du ms
*/
void delay(ULONG time)
{
    ULONG j; /* time*/
    for(j=0U; j<time; j++){ /* ...  */
        __delay_cycles(1000); /* ...  */
	} 
}

/*
initialise le servo moteur
*/
void init_servo(void)
{
    P1DIR |= BIT2; 
    P1SEL |= BIT2; 
    TACTL = TASSEL_2 | MC_1; 
    TACCTL1 |= OUTMOD_7; 
    TACCR0 = 20000; 
    TACCR1 = 1500; 
}
/*
mets le servomoteur � 180�
*/
void servo_left(void)
{
    SINT_32 i; /* increment the loop*/
    for(i=1000;i<2000;i=i+100)
     {
        TACCR1 = i;
        delay(100);
     }
}
/*
mets le servomoteur � 0�
*/

void servo_right(void)
{
    SINT_32 i; /* increment the loop*/

    for(i=2000;i>1000;i=i-100)
     {
        TACCR1 = i;
        delay(100);
     }
}
    
/*
 mets le servomoteur � angle�
*/
UINT_32 servo_set(UINT_32 angle){
	UINT_32 r;
    if(angle<30U){
        TACCR1=500;/* 0� */
	r = 0U;
    }
    else if(angle<60U){
        TACCR1=1000;/* 45� */
	r = 45U;
    }
    else if(angle<120U){
        TACCR1=1500;/* 90� */
	r = 90U;
    }
    else if(angle<150U){
        TACCR1=2000;/* 135� */
	r = 135U;
    }
    else if(angle<=180U){
        TACCR1=2500;/* 180� */
	r = 180U;
    }
    else{
        TACCR1=1500;/* 90� */
	r = 90U;
    }
	return r;
}
