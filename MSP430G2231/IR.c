/*
 * IR.c
 *
 *  Created on: Mar 24, 2021
 *      Author: Shesley LECKPA
 */

#include "msp430.h"
#include "ADC.h"
#include "typedef.h"

UINT_32 distance_measure(UCHAR v)
{
    UINT_32 d=0U;
	SINT_32 x=0;

    x=ADC_code(v);

    if(x<61){
        d=36U;
    }

    else if(x<82){
        d=32U;
    }

    else if(x<102){
        d=28U;
    }

    else if(x<123){
        d=24U;
    }

    else if(x<143){
        d=20U;
    }

    else if(x<164){
        d=16U;
    }

    else if(x<225){
        d=12U;
    }

    else if(x<327){
        d=8U;
    }

    else if(x<553){
        d=4U;
    }

    else{
        d=0U;
    }
    return d;
}
