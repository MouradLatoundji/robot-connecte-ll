/*
 * interpreter.c
 *
 *  Created on: 11 mars 2021
 *  Author: mourad - shesley
 */


#include <interpreter.h>


/* Text interpreter */
void interpreter_UART(void){

    if( (strcmp((const char *)cmd, "h") == 0) || (strcmp((const char *)cmd, "help") == 0) )
    {
      command=HELP;
    }
    else if ( (strcmp((const char *)cmd, "b") == 0) || (strcmp((const char *)cmd, "stop") == 0) )
    {
      command=STOP;
    }
    else if ( (strcmp((const char *)cmd, "i") == 0) || (strcmp((const char *)cmd, "forward") == 0) )
    {
      command=FORWARD;
    }
    else if ( (strcmp((const char *)cmd, "j") == 0) || (strcmp((const char *)cmd, "left") == 0) )
    {
      command=LEFT_ROTATION;
    }
    else if ( (strcmp((const char *)cmd, "l") == 0) || (strcmp((const char *)cmd, "right") == 0) )
    {
      command=RIGHT_ROTATION;
    }
    else if ( (strcmp((const char *)cmd, "k") == 0) || (strcmp((const char *)cmd, "back") == 0) )
    {
      command=BACK;
    }
    else if ( (strcmp((const char *)cmd, "z") == 0) || (strcmp((const char *)cmd, "step forward") == 0) )
    {
       command=STEP_FORWARD;
    }
    else if ( (strcmp((const char *)cmd, "q") == 0) || (strcmp((const char *)cmd, "step left") == 0) )
    {
      command=STEP_LEFT_ROTATION;
    }
    else if ( (strcmp((const char *)cmd, "d") == 0) || (strcmp((const char *)cmd, "step right") == 0) )
    {
      command=STEP_RIGHT_ROTATION;
    }
    else if ( (strcmp((const char *)cmd, "s") == 0) || (strcmp((const char *)cmd, "step back") == 0) )
    {
      command=STEP_BACK;
    }
    else if ( (strcmp((const char *)cmd, "a") == 0) || (strcmp((const char *)cmd, "start") == 0) )
     {
      command=RUNNING_ON;
     }
    else if ( (strcmp((const char *)cmd, "e") == 0) || (strcmp((const char *)cmd, "end") == 0) )
    {
      command=RUNNING_OFF;
    }
    else if ( (strcmp((const char *)cmd, "m") == 0) || (strcmp((const char *)cmd, "manual") == 0) )
    {
      command=MAN;
    }
    else if ( (strcmp((const char *)cmd, "p") == 0) || (strcmp((const char *)cmd, "auto") == 0) )
    {
      command=AUTO;
    }
    else if ( (strcmp((const char *)cmd, "w") == 0) || (strcmp((const char *)cmd, "scan") == 0) )
    {
      command=SCAN;
    }
    else if ( (strcmp((const char *)cmd, "n") == 0) || (strcmp((const char *)cmd, "scan all") == 0) )
    {
      command=SCAN_ALL;
    }
    else if ( (strcmp((const char *)cmd, "c") == 0) || (strcmp((const char *)cmd, "redress") == 0) )
    {
      command=REDRESS;
    }
    else if ( (strcmp((const char *)cmd, "x") == 0) || (strcmp((const char *)cmd, "left sweep 90") == 0) )
    {
      command=LEFT_SWEEP_90;
    }
    else if ( (strcmp((const char *)cmd, "v") == 0) || (strcmp((const char *)cmd, "right sweep 90") == 0) )
    {
      command=RIGHT_SWEEP_90;
    }
    else if ( (strcmp((const char *)cmd, "f") == 0) || (strcmp((const char *)cmd, "left sweep") == 0) )
    {
      command=LEFT_SWEEP;
    }
    else if ( (strcmp((const char *)cmd, "g") == 0) || (strcmp((const char *)cmd, "right sweep") == 0) )
    {
      command=RIGHT_SWEEP;
    }
    else
    {
      command=ERR;
    }
}

/* Distance interpreter */
void interpreter_SPI(void){

    switch(distance_char){
    case '9':
         /// distance_obs="36";
        test_toggle_LED(9);
    break;
    case '8':
          distance_obs="32";
    break;
    case '7':
          distance_obs="28";
    break;
    case '6':
          distance_obs="24";
    break;
    case '5':
          distance_obs="20";
    break;
    case '4':
          distance_obs="16";
    break;
    case '3':
          distance_obs="12";
    break;
    case '2':
          distance_obs="8";
    break;
    case '1':
          distance_obs="4";
    break;
    case '0':
          distance_obs="0";
    break;
    default:
          distance_obs="";
    break;

 }

    distance_obs_ang[angle_servo]=distance_char;

    if(distance_obs_ang[ANG_90]<=DISTANCE_OBS_MIN){
        command_2231=STOP;
    }
    else{
        command_2231=NONE;
    }

}
