/*
 * spi.h
 *
 *  Created on: 21 mars 2021
 *      Author: moura
 */

#ifndef SPI_H_
#define SPI_H_

#include <msp430g2553.h>
#include <def_2553.h>


void RXdata_spi(unsigned char *c);
void TXdata_spi(unsigned char c);



#endif /* SPI_H_ */
