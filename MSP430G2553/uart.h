/*
 * uart.h
 *
 *  Created on: 11 mars 2021
 *      Author: mourad - shesley
 */

#ifndef UART_H_
#define UART_H_


#include <msp430g2553.h>
#include <def_2553.h>
#include <ressources.h>

void RXdata(unsigned char *c);
void TXdata(unsigned char c);
void send_msg_UART(const char *msg);
void display_help(void);
int display_position_servo(ANGLE angle);

#endif /* UART_H_ */
