/*
 * def_2553.h
 *
 *  Created on: 11 mars 2021
 *  Author: mourad - shesley
 */

#ifndef DEF_2553_H_
#define DEF_2553_H_


typedef enum{FALSE=0,TRUE=1} BOOLEAN;
typedef enum {MANUAL,AUTONOMOUS} MODE;
typedef enum {NONE,HELP,FORWARD,LEFT_ROTATION,RIGHT_ROTATION,BACK,
            STEP_FORWARD,STEP_LEFT_ROTATION,STEP_RIGHT_ROTATION,STEP_BACK,
            SCAN,SCAN_ALL,LEFT_SWEEP,LEFT_SWEEP_90,RIGHT_SWEEP,RIGHT_SWEEP_90,REDRESS,
            STOP,RUNNING_ON,RUNNING_OFF,AUTO,MAN,ERR} COMMAND;

typedef enum {ANG_180,ANG_135,ANG_90,ANG_45,ANG_0} ANGLE;

/* Digital I/O Port1 */
#define BIT_DIR_A BIT1
#define BIT_DIR_B BIT5
#define BIT_PWM_A BIT2
#define BIT_PWM_B BIT4

/* PWM control */
#define PERIOD_PWM 100
#define LOW_SPEED 25
#define MEDIUM_SPEED 55
#define HIGH_SPEED 85

/* UART Port1 */
#define RXD BIT2
#define TXD BIT1

/* ASCII Commands */
#define LF      0x0A            /* line feed or \n */
#define CR      0x0D            /* carriage return or \r*/
#define ESC     0x1B            /* escape */
#define BSPC    0x08            /* back space */
#define DEL     0x7F            /* suppress */

/* SPI linew */
#define CS          BIT4            /* chip select for SPI Master */
#define SCK         BIT5            /* Serial Clock */
#define DATA_OUT    BIT6            /* DATA out */
#define DATA_IN     BIT7            /* DATA in */

/*LED1*/
#define LED BIT0

/* Limiters const */
#define CMDLEN_MAX 30
#define MOVETIME_MAX 20000
#define MOVESPEED_MAX 99
#define DISTANCE_OBS_MIN '2'
#define DISTANCE_OBS_MAX '7'


#endif /* DEF_2553_H_ */
