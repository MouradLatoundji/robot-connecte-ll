/*
 * servomotors.c
 *
 *  Created on: 11 mars 2021
 *  Author: mourad - shesley
 */


#include <motors.h>

/* Forward steering */
void forward( void )
{
    P2OUT&=~BIT_DIR_A;
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=HIGH_SPEED;
    TA1CCR2=HIGH_SPEED;
}

/* Left turn steering */
void left( void )
{
    P2OUT&=~BIT_DIR_A;
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=LOW_SPEED;
    TA1CCR2=HIGH_SPEED;
}

/* Right turn steering */
void right( void )
{
    P2OUT&=~BIT_DIR_A;
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=HIGH_SPEED;
    TA1CCR2=LOW_SPEED;
}

/* Back steering */
void back( void )
{
    P2OUT&=~BIT_DIR_B;
    P2OUT|=(BIT_DIR_A);
    TA1CCR1=HIGH_SPEED;
    TA1CCR2=HIGH_SPEED;
}

/* Stop motors */
void stop( void )
{
    TA1CCR1=0;
    TA1CCR2=0;
}

/* Left rotation steering */
void left_rotation( void )
{
    P2OUT|=(BIT_DIR_A);
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=HIGH_SPEED;
    TA1CCR2=HIGH_SPEED;
}

/* Right rotation steering */
void right_rotation( void )
{
    P2OUT&=~BIT_DIR_A;
    P2OUT&=~BIT_DIR_B;
    TA1CCR1=HIGH_SPEED;
    TA1CCR2=HIGH_SPEED;
}

/* Delay time milliseconds */
void delay(unsigned long time)
{
    unsigned int i;
    for(i=0U;i<time;i++){
    __delay_cycles(1000U);
    }
}

/*
 * Allows to check to obtain a value between two bounds
 * Return max when var is greater than max
 * Return min when var is less than min
 */
unsigned long frame(unsigned long var,unsigned long min,unsigned long max)
{
    if(var<min){
        var=min;
    }
    else if(var>max){
        var=max;
    }
    else{
        /*...*/
    }

    return var;
}

/* move forward control */
void move_forward(unsigned long speed, unsigned long time)
{
    speed=frame(speed, 1, MOVESPEED_MAX);
    time=frame(time,1,MOVETIME_MAX);
    P2OUT&=~BIT_DIR_A;
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=(int)speed;
    TA1CCR2=(int)speed;
    delay(time);
    stop();
    delay(100);
}

/* move back control */
void move_back(unsigned long speed, unsigned long time)
{
    speed=frame(speed, 1, MOVESPEED_MAX);
    time=frame(time,1,MOVETIME_MAX);
    P2OUT&=~BIT_DIR_B;
    P2OUT|=(BIT_DIR_A);
    TA1CCR1=speed;
    TA1CCR2=speed;
    delay(time);
    stop();
    delay(100);
}

/* move left rotation control */
void move_left_rotation(unsigned long speed, unsigned long time)
{
    speed=frame(speed, 1, MOVESPEED_MAX);
    time=frame(time,1,MOVETIME_MAX);
    P2OUT|=(BIT_DIR_A);
    P2OUT|=(BIT_DIR_B);
    TA1CCR1=speed;
    TA1CCR2=speed;
    delay(time);
    stop();
    delay(100);

}

/* move right rotation control */
void move_right_rotation(unsigned long speed, unsigned long time)
{
    speed=frame(speed, 1, MOVESPEED_MAX);
    time=frame(time,1,MOVETIME_MAX);
    P2OUT&=~BIT_DIR_A;
    P2OUT&=~BIT_DIR_B;
    TA1CCR1=speed;
    TA1CCR2=speed;
    delay(time);
    stop();
    delay(100);
}

